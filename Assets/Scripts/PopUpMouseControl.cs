﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpMouseControl : MonoBehaviour
{
    public GameObject popUpManager;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if (Physics.Raycast(ray, out hit, 0.0f))
            {
                if (hit.transform != null)
                {
                    print(hit.transform.gameObject);
                    popUpManager.GetComponent<PopUpScript>().closePopUp();
                }
                else {
                    print("hit not detected");
                }
            }
        }
    }
}
