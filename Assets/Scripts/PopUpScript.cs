﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopUpScript : MonoBehaviour
{
    public GameObject popUp;
    public GameObject currentExhibit;
    public GameObject currentArtifact;
    public GameObject currentTomb;
    public GameObject reticle;
    private GameObject[] artifacts;
    private bool isOpen;

    // Start is called before the first frame update
    void Start()
    {
        popUp.SetActive(false);
        isOpen = false;
    }

    // Update is called after each frame
    void Update()
    {
        if(isOpen && (currentArtifact == null))
        {
            if (Input.GetKey(KeyCode.Alpha1))
            {
                openArtifact(artifacts[0]);
            }
            else if (Input.GetKey(KeyCode.Alpha2))
            {
                openArtifact(artifacts[1]);
            }
            else if (Input.GetKey(KeyCode.Alpha3))
            {
                openArtifact(artifacts[2]);
            }
            else if (Input.GetKey(KeyCode.Alpha4))
            {
                openArtifact(artifacts[3]);
            }
            else if (Input.GetKey(KeyCode.Alpha5))
            {
                openArtifact(artifacts[4]);
            }
            else if (Input.GetKey(KeyCode.Alpha6))
            {
                openArtifact(artifacts[5]);
            }
            else if (Input.GetKey(KeyCode.Alpha7))
            {
                openArtifact(artifacts[6]);
            }
            else if (Input.GetKey(KeyCode.Alpha8))
            {
                openArtifact(artifacts[7]);
            }
            else if (Input.GetKey(KeyCode.Alpha9))
            {
                openArtifact(artifacts[8]);
            }
            else if (Input.GetKey(KeyCode.Alpha0))
            {
                openArtifact(artifacts[9]);
            }
            else if (Input.GetKey(KeyCode.T) && currentExhibit.GetComponent<ArrayScript>().tomb != null) {
                openTomb();
            }
            else if(Input.GetKey(KeyCode.N) && currentExhibit.GetComponent<ArrayScript>().nextPanel != null) {
                nextPanel();
            }
            else if(Input.GetKey(KeyCode.P) && currentExhibit.GetComponent<ArrayScript>().prevPanel != null) {
                prevPanel();
            }
            else if (Input.GetKey(KeyCode.B) && (currentArtifact != null))
            {
                closeArtifact();
            }
            else if (Input.GetKey(KeyCode.B) && (currentTomb != null)) {
                closeTomb();
            }
        } else if (Input.GetKey(KeyCode.B) && (currentArtifact != null) && isOpen) {
            closeArtifact();
        } else if (Input.GetKey(KeyCode.B) && (currentTomb != null) && isOpen) {
            closeTomb();
        }
    }

    public void nextPanel() {
        GameObject nextPanel = currentExhibit.GetComponent<ArrayScript>().nextPanel;
        closePopUp();
        openPopUp(nextPanel);
    }
    public void prevPanel() {
        GameObject prevPanel = currentExhibit.GetComponent<ArrayScript>().prevPanel;
        closePopUp();
        openPopUp(prevPanel);
    }

    /*
     * This opens the canvas assigned as popUp defined in PopUp_Manager
     */
    public void openPopUp(GameObject exhibit)
    {
        currentExhibit = exhibit;
        for(int i = 0; i < currentExhibit.GetComponent<ArrayScript>().englishElements.Length; i++) {
            currentExhibit.GetComponent<ArrayScript>().englishElements[i].SetActive(CustomSettings.IsEnglish);
        }
        for(int i = 0; i < currentExhibit.GetComponent<ArrayScript>().spanishElements.Length; i++) {
            currentExhibit.GetComponent<ArrayScript>().spanishElements[i].SetActive(!CustomSettings.IsEnglish);
        }
        updateArtifacts();
        // Cursor.lockState = CursorLockMode.None;
        popUp.SetActive(true);
        exhibit.SetActive(true);
        isOpen = true;
        //reticle.SetActive(false);

    }

    /*
     * This closes the canvas assigned as popUp defined in PopUp_Manager
     */
    public void closePopUp() 
    {
        if(currentArtifact != null) {
            closeArtifact();
        }
        if(currentTomb != null) {
            closeTomb();
        }
        if(currentExhibit != null)
        {
            currentExhibit.SetActive(false);
            artifacts = null;
        }
        popUp.SetActive(false);
        // Cursor.lockState = CursorLockMode.Locked;
        isOpen = false;
        //reticle.SetActive(true);
    }

    public void openArtifact(GameObject keyPressed)
    {
        currentArtifact = keyPressed;
        if(currentArtifact != null)
        {
            currentExhibit.SetActive(false);
            currentArtifact.SetActive(true);
            for(int i = 0; i < currentArtifact.GetComponent<ArrayScript>().englishElements.Length; i++) {
                currentArtifact.GetComponent<ArrayScript>().englishElements[i].SetActive(CustomSettings.IsEnglish);
            }
            for(int i = 0; i < currentArtifact.GetComponent<ArrayScript>().spanishElements.Length; i++) {
                currentArtifact.GetComponent<ArrayScript>().spanishElements[i].SetActive(!CustomSettings.IsEnglish);
            }
        }
    }

    public void openTomb() {
        currentTomb = currentExhibit.GetComponent<ArrayScript>().tomb;
        if(currentTomb != null)
        {
            currentExhibit.SetActive(false);
            currentTomb.SetActive(true);
            for(int i = 0; i < currentTomb.GetComponent<ArrayScript>().englishElements.Length; i++) {
                if(currentTomb.GetComponent<ArrayScript>().englishElements[i] != null) {
                    currentTomb.GetComponent<ArrayScript>().englishElements[i].SetActive(CustomSettings.IsEnglish);
                }
            }
            for(int i = 0; i < currentTomb.GetComponent<ArrayScript>().spanishElements.Length; i++) {
                if(currentTomb.GetComponent<ArrayScript>().spanishElements[i] != null) {
                    currentTomb.GetComponent<ArrayScript>().spanishElements[i].SetActive(!CustomSettings.IsEnglish);
                }
            }
        }
    }

    public void closeArtifact()
    {
        currentArtifact.SetActive(false);
        currentExhibit.SetActive(true);
        currentArtifact = null;
    }

    public void closeTomb() {
        currentTomb.SetActive(false);
        currentExhibit.SetActive(true);
        currentTomb = null;
    }

    private void updateArtifacts()
    {
        artifacts = currentExhibit.GetComponent<ArrayScript>().artifacts;
    }
}
