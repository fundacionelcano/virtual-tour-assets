﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{
    public GameObject TourSelectScreen;
    public GameObject LangSelectScreen;
    public GameObject TipsAndTricksScreen;
    public void StartMuseumTour() {
        Cursor.lockState = CursorLockMode.Locked;
        SceneManager.LoadScene("Museum");
    }

    public void StartParkTour() {
        Cursor.lockState = CursorLockMode.Locked;
        CustomSettings.CurrentHotSpot = 1;
        SceneManager.LoadScene("Outdoor");
    }

    public void SwitchLanguage() {
       CustomSettings.IsEnglish = !CustomSettings.IsEnglish;
    }

    public void SetSpanish() {
        CustomSettings.IsEnglish = false;
        LangSelectScreen.SetActive(false);
        TipsAndTricksScreen.SetActive(true);
        UpdateLang();
    }

    public void SetEnglish() {
        CustomSettings.IsEnglish = true;
        LangSelectScreen.SetActive(false);
        TipsAndTricksScreen.SetActive(true);
        UpdateLang();
    }

    public void UpdateLang() {
        for(int i = 0; i < TourSelectScreen.GetComponent<LanguageSwitcher>().englishElements.Length; i++) {
            if(TourSelectScreen.GetComponent<LanguageSwitcher>().englishElements[i] != null) {
                TourSelectScreen.GetComponent<LanguageSwitcher>().englishElements[i].SetActive(CustomSettings.IsEnglish);
            }
        }
        for(int i = 0; i < TourSelectScreen.GetComponent<LanguageSwitcher>().spanishElements.Length; i++) {
            if(TourSelectScreen.GetComponent<LanguageSwitcher>().spanishElements[i] != null) {
                TourSelectScreen.GetComponent<LanguageSwitcher>().spanishElements[i].SetActive(!CustomSettings.IsEnglish);
            }
        }
        for(int i = 0; i < TipsAndTricksScreen.GetComponent<LanguageSwitcher>().englishElements.Length; i++) {
            if(TipsAndTricksScreen.GetComponent<LanguageSwitcher>().englishElements[i] != null) {
                TipsAndTricksScreen.GetComponent<LanguageSwitcher>().englishElements[i].SetActive(CustomSettings.IsEnglish);
            }
        }
        for(int i = 0; i < TipsAndTricksScreen.GetComponent<LanguageSwitcher>().spanishElements.Length; i++) {
            if(TipsAndTricksScreen.GetComponent<LanguageSwitcher>().spanishElements[i] != null) {
                TipsAndTricksScreen.GetComponent<LanguageSwitcher>().spanishElements[i].SetActive(!CustomSettings.IsEnglish);
            }
        }
        
    }

    public void LoadTourSelectScreen() {
        TipsAndTricksScreen.SetActive(false);
        TourSelectScreen.SetActive(true);
    }
}
