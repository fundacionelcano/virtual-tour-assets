﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrayScript : MonoBehaviour
{
    public GameObject[] artifacts;
    /*
    public GameObject englishTitle;
    public GameObject spanishTitle;
    public GameObject englishText;
    public GameObject spanishText;
    public GameObject englishButton;
    public GameObject spanishButton;
    */
    public GameObject[] englishElements;
    public GameObject[] spanishElements;
    public GameObject nextPanel;
    public GameObject prevPanel;
    public GameObject tomb;
}
