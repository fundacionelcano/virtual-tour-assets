﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class OutdoorHUDScript : MonoBehaviour
{
    public GameObject[] englishElements;
    public GameObject[] spanishElements;
    public GameObject mapPanel;
    public GameObject[] arrows;
    private int lastArrow = 1;
    public bool isEnglish = true;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.H)) {
            Cursor.lockState = CursorLockMode.None;
            SceneManager.LoadScene("MainMenu");
        }
        if(Input.GetKey(KeyCode.M)) {
            mapPanel.SetActive(true);
        }
        if(Input.GetKey(KeyCode.C) && mapPanel.activeSelf) {
            mapPanel.SetActive(false);
        }
        if(isEnglish != CustomSettings.IsEnglish) {
            isEnglish = CustomSettings.IsEnglish;
            for(int i = 0; i < englishElements.Length; i++){
                englishElements[i].SetActive(CustomSettings.IsEnglish);
            }
            for(int i = 0; i < spanishElements.Length; i++){
                spanishElements[i].SetActive(!CustomSettings.IsEnglish);
            }
        }
        UpdateArrows();
    }

    void UpdateArrows() {
        if(lastArrow != CustomSettings.CurrentHotSpot) {
            if(arrows[CustomSettings.CurrentHotSpot - 1] != null) {
                arrows[lastArrow - 1].SetActive(false);
                arrows[CustomSettings.CurrentHotSpot - 1].SetActive(true);
                lastArrow = CustomSettings.CurrentHotSpot;
            }
        }
    }

}
