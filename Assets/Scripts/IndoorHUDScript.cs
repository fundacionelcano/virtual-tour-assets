﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IndoorHUDScript : MonoBehaviour
{
    public GameObject[] englishElements;
    public GameObject[] spanishElements;
    // Start is called before the first frame update
    public bool isEnglish = true;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.H)) {
            Cursor.lockState = CursorLockMode.None;
            SceneManager.LoadScene("MainMenu");
        }
        if(isEnglish != CustomSettings.IsEnglish) {
            isEnglish = CustomSettings.IsEnglish;
            for(int i = 0; i < englishElements.Length; i++){
                englishElements[i].SetActive(CustomSettings.IsEnglish);
            }
            for(int i = 0; i < spanishElements.Length; i++){
                spanishElements[i].SetActive(!CustomSettings.IsEnglish);
            }
        }
    }

}
