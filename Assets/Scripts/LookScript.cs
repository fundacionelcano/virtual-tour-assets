﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.AccessControl;
using System.Security.Cryptography;
using UnityEngine;

public class LookScript : MonoBehaviour
{
    Vector2 rotation = new Vector2(0, 0);
    public float speed;
    private bool locked = true;
    public GameObject popUp;
    private bool isPopUpActive;

    public float clampAngle = 75.0F;
    private float rotX;
    private float rotY;
    // Start is called before the first frame update
    void Start()
    {
        rotX = rotation.x;
        rotY = rotation.y;
    }

    // Update is called once per frame
    void Update()
    { 
        if(popUp.activeSelf == true)
        {
            isPopUpActive = true;
        } else
        {
            isPopUpActive = false;
        }
        /*
        if (Input.GetKey(KeyCode.Space) && !isPopUpActive)
        {
            if(Cursor.lockState == CursorLockMode.None)
            {
                Cursor.lockState = CursorLockMode.Locked;
                locked = true;
            }
            else
            {
                Cursor.lockState = CursorLockMode.None;
                locked = false;
            }
        }
        */
        if (locked && !isPopUpActive)
        {
            // rotation.x += Input.GetAxis("Mouse Y") * -1 * speed;
            // rotation.y += Input.GetAxis("Mouse X") * speed;
            // if(rotation.x < -360F) {
            //     rotation.x += 360F;
            //     rotation.x = Mathf.Clamp(rotation.x, -360F, 360F);
            // }
            // if(rotation.x > 360F) {
            //     rotation.x -= 360F;
            //     rotation.x = Mathf.Clamp(rotation.x, -360F, 360F);
            // }
            // if(rotation.y < -360F) {
            //     rotation.y += 360F;
            //     rotation.y = Mathf.Clamp(rotation.y, -60, 60F);
            // }
            // if(rotation.y > 360F) {
            //     rotation.y -= 360F;
            //     rotation.y = Mathf.Clamp(rotation.y, -60F, 60F);
            // }
            // transform.eulerAngles = (Vector2)rotation;
            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = Input.GetAxis("Mouse Y");

            rotY += mouseX * speed;
            rotX += mouseY * speed * -1;

            rotX = Mathf.Clamp(rotX, -clampAngle, clampAngle);

            Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0.0f);
            transform.rotation = localRotation;
        }
    }
}
